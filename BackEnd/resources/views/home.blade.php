@extends('Layouts.app')
@section('title') <span>Bajar Data</span> @endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-10 pb-4">
            <div class="card p-3">
                <div class="row">

                    <div class="col-4">
                        <label>Cuentas: </label>
                        <select class="form-select" id="account" data-placeholder="Choose one thing">
                            <option selected disabled>Seleccionar</option>
                            @forelse($data as $item)
                                <option value="">{{ $item }}</option>
                            @empty
                                <p>Sin resultados</p>
                            @endforelse
                        </select>
                    </div>

                    <div class="col-4">
                        <label>Campañas: </label>
                        <select class="form-select" id="campaign">
                            <option selected disabled>Seleccionar</option>
                            {{--  @foreach ($campains as $item)
                                <option value="{{$item['id']}}">{{ $item['name'] }}</option>
                            @endforeach  --}}

                        </select>
                    </div>

                    <div class="col-4">
                        <label>Grupo de anuncios: </label>
                        <select class="form-select" id="adGroup" data-placeholder="Choose one thing">
                            <option selected disabled>Seleccionar</option>
                        </select>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-10 table-responsive">
            <h3 class="text-bold">Palabras clave</h3>
            <table class="table" id="tableKeywords">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">NOMBRE</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="2">
                            <h1 class="text-center text-danger mt-5">Sin registros</h1>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    @section('script')
        <script type="text/javascript">
            $(document).ready(function() {
                let accountSelect = $('#account');
                let campaignSelect = $('#campaign');
                let adGroupSelect = $('#adGroup');
                let keywordsTable = $('#tableKeywords tbody')

                let curstomerId = 0;
                let idCampaign = 0;

                accountSelect.change(function() {
                    curstomerId = '5252172223';
                    campaignSelect.empty();
                    adGroupSelect.empty();
                    if (curstomerId) {
                        $.ajax({
                            url: "{{ route('campaings') }}",
                            type: 'GET',
                            data: {
                                customerId: curstomerId
                            },
                            dataType: 'json',
                            success: function(response) {
                                campaignSelect.append(
                                    '<option value="">-- Seleccione una campaña</option>')
                                $.each(response, function(key, value) {
                                    console.log(value)
                                    campaignSelect.append("<option value='" + value.id +
                                        "'>" + value.name + "</option>");
                                });
                            },
                            error: function() {
                                alert('Hubo un error obteniendo las campañas!');
                            }
                        });
                    }
                });

                campaignSelect.change(function() {
                    idCampaign = $(this).val();
                    adGroupSelect.empty();
                    keywordsTable.empty();
                    if (idCampaign) {
                        $.ajax({
                            url: "{{ route('adGroups') }}",
                            type: 'GET',
                            data: {
                                customerId: curstomerId,
                                campaignId: idCampaign
                            },
                            dataType: 'json',
                            success: function(response) {
                                console.log(response);
                                adGroupSelect.append(
                                    '<option value="">-- Seleccione un grupo de anuncios</option>'
                                )
                                $.each(response, function(key, value) {
                                    console.log(value)
                                    adGroupSelect.append("<option value='" + value.id +
                                        "'>" + value.name + "</option>");
                                });
                            },
                            error: function() {
                                alert('Hubo un error obteniendo los grupos de anuncios!');
                            }
                        });
                    }
                });

                adGroupSelect.change(function() {
                    let adGroup = $(this).val();
                    if (adGroup) {
                        $.ajax({
                            url: "{{ route('keywords') }}",
                            type: 'GET',
                            data: {
                                customerId: curstomerId,
                                adGroupId: adGroup
                            },
                            dataType: 'json',
                            success: function(response) {
                                keywordsTable.empty();
                                $.each(response, function(key, value) {
                                    console.log(value)
                                    keywordsTable.append(`
                                    <tr>
                                        <td>${value.id}</td>
                                        <td>${value.name}</td>
                                    </tr>
                                    `);
                                });
                            },
                            error: function(err) {
                                console.log(err);
                                alert('Hubo un error obteniendo las palabras!');
                            }
                        })
                    }
                })
            });
        </script>
    @endsection
@endsection
