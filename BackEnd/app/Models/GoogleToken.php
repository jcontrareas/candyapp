<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GoogleToken extends Model
{
    use HasFactory;
    protected $fillable = [
        'client_id',
        'client_secret',
        'refresh_token',
        'token'
    ];
}
