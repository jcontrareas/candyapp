<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Http\Resources\Home\AdGroupsResources;
use App\Http\Resources\Home\CampaingsResources;
use App\Http\Resources\Home\CustomersResources;
use App\Http\Resources\Home\KeywordsResources;
use Google\Ads\GoogleAds\V11\Resources\CustomerClient;
use Google\Ads\GoogleAds\Lib\V11\GoogleAdsClient;
use Google\Ads\GoogleAds\V11\Services\CustomerServiceClient;


use Illuminate\Http\Request;

class HomeController extends Controller
{
    private const PAGE_SIZE = 1000;
    private const MANAGER_CUSTOMER_ID = '6963202607';
    private static $rootCustomerClients = [];
    private static $iterateCustomerClients = [];

    /**
     * It returns a list of campaigns.
     * 
     * @param Request request The request object.
     * @param GoogleAdsClient googleAdsClient The Google Ads client.
     * 
     * @return An array of objects with the id and name of the campaigns.
     */
    public function getCampaings(Request $request, GoogleAdsClient $googleAdsClient)
    {
        $campains = [];
        $customerClient = $googleAdsClient->getGoogleAdsServiceClient();

        $query = 'SELECT campaign.id, campaign.name FROM campaign ORDER BY campaign.id';

        $stream = $customerClient->searchStream($request->customerId, $query);
        $campanas = collect($stream->iterateAllElements());

        foreach ($campanas as $key => $googleAdsRow) {
            array_push($campains, array('id' => $googleAdsRow->getCampaign()->getId(), 'name' => $googleAdsRow->getCampaign()->getName()));
        }

        return CampaingsResources::collection($campains);
    }


    /**
     * It gets all the ad groups for a given campaign
     * 
     * @param Request request The request object that contains the campaign ID.
     * @param GoogleAdsClient googleAdsClient The Google Ads client.
     * 
     * @return An array of ad groups
     */
    public function getAdGroups(Request $request, GoogleAdsClient $googleAdsClient)
    {
        $adGroups = [];
        $customerClient = $googleAdsClient->getGoogleAdsServiceClient();

        $query = "SELECT campaign.id, ad_group.id, ad_group.name FROM ad_group WHERE campaign.id = $request->campaignId";

        $response = $customerClient->search($request->customerId, $query, ['pageSize' => self::PAGE_SIZE]);

        $groupsAds = collect($response->iterateAllElements());

        foreach ($groupsAds as $item) {
            array_push($adGroups, array('id' => $item->getAdGroup()->getId(), 'name' => $item->getAdGroup()->getName()));
        }

        return AdGroupsResources::collection($adGroups);
    }


    /**
     * It gets all the keywords from the Google Ads account and returns them as a collection of
     * resources
     * 
     * @param Request request The request object that contains the customer ID and ad group ID.
     * @param GoogleAdsClient googleAdsClient The Google Ads client.
     * 
     * @return An array of keywords
     */
    public function getKeywords(Request $request, GoogleAdsClient $googleAdsClient)
    {
        $adkeywords = [];
        $customerClient = $googleAdsClient->getGoogleAdsServiceClient();

        $query =
            'SELECT ad_group.id, '
            . 'ad_group_criterion.type, '
            . 'ad_group_criterion.criterion_id, '
            . 'ad_group_criterion.keyword.text, '
            . 'ad_group_criterion.keyword.match_type '
            . 'FROM ad_group_criterion '
            . 'WHERE ad_group_criterion.type = KEYWORD';

        if ($request->adGroupId !== null) {
            $query .= " AND ad_group.id = $request->adGroupId";
        }

        $response = $customerClient->search($request->customerId, $query, ['pageSize' => self::PAGE_SIZE]);

        $keywordsAds = collect($response->iterateAllElements());

        foreach ($keywordsAds as $item) {
            array_push($adkeywords, array(
                'id' => $item->getAdGroup()->getId(),
                'name' => $item->getAdGroupCriterion()->getKeyword()->getText()
            ));
        }

        return KeywordsResources::collection($adkeywords);
    }

    // ---------------------------------------------------------------

    /**
     * It takes the root customer ID, and then it gets all the child accounts of that root customer ID,
     * and then it gets all the child accounts of those child accounts, and so on, until it has a
     * complete list of all the accounts in the hierarchy
     * 
     * @param Request request The request object.
     * @param GoogleAdsClient googleAdsClient The GoogleAdsClient object that contains the credentials
     * to access the Google Ads API.
     * 
     * @return an array of all the accounts that the user has access to.
     */
    public static function getAccount(Request $request, GoogleAdsClient $googleAdsClient)
    {
        $rootCustomerIds = [];
        $allHierarchies = [];
        $accountsWithNoInfo = [];

        if (is_null(self::MANAGER_CUSTOMER_ID)) {
            $rootCustomerIds = self::getAccessibleCustomers($googleAdsClient);
        } else {
            $rootCustomerIds[] = self::MANAGER_CUSTOMER_ID;
        }

        foreach ($rootCustomerIds as $item) {
            $customerHierarchy = self::customerHierarchy($item, $googleAdsClient);
            if (is_null($customerHierarchy)) {
                $accountsWithNoInfo[] = $item;
            } else {
                $allHierarchies += $customerHierarchy;
            }
        }
        foreach ($allHierarchies as $rootCustomerId => $customerIdsToChildAccounts) {
            self::printAccountHierarchy(
                self::$rootCustomerClients[$rootCustomerId],
                $customerIdsToChildAccounts,
                0
            );
        }
        return CustomersResources::collection(self::$iterateCustomerClients);
    }

    /**
     * > This function takes a CustomerClient object, an array of child accounts, and a depth value. It
     * then adds the CustomerClient object to an array of CustomerClient objects, and if the
     * CustomerClient object has child accounts, it calls itself with the child accounts
     * 
     * @param CustomerClient customerClient The customer client object.
     * @param array childAccounts An array of child accounts, where the key is the customer ID of the
     * parent account.
     * @param int depth The depth of the current account in the hierarchy.
     */
    private static function printAccountHierarchy(
        CustomerClient $customerClient,
        array $childAccounts,
        int $depth
    ) {

        $customerId = $customerClient->getId();
        array_push(self::$iterateCustomerClients, array(
            'id' => $customerId,
            'name' => $customerClient->getDescriptiveName()
        ));

        if (array_key_exists($customerId, $childAccounts)) {
            foreach ($childAccounts[$customerId] as $childAccount) {
                self::printAccountHierarchy($childAccount, $childAccounts, $depth + 1);
            }
        }
    }

    /**
     * It takes a root customer ID and returns a map of customer IDs to child accounts
     * 
     * @param int rootCustomerId The ID of the root customer.
     * @param googleAdsClient The Google Ads API client.
     * 
     * @return ?array an array of customer IDs to child accounts.
     */
    private static function customerHierarchy(
        int $rootCustomerId,
        $googleAdsClient
    ): ?array {

        $rootCustomerClient = null;
        $customerIdsToChildAccounts = [];
        $managerCustomerIdsToSearch = [$rootCustomerId];

        $googleAdsServiceClient = $googleAdsClient->getGoogleAdsServiceClient();
        
        $query = 'SELECT customer_client.client_customer, customer_client.level,'
        . ' customer_client.manager, customer_client.descriptive_name,'
        . ' customer_client.currency_code, customer_client.time_zone,'
        . ' customer_client.id FROM customer_client WHERE customer_client.level <= 1';

        while (!empty($managerCustomerIdsToSearch)) {
            $customerIdToSearch = array_shift($managerCustomerIdsToSearch);

            $stream = $googleAdsServiceClient->searchStream(
                $customerIdToSearch,
                $query
            );

            foreach ($stream->iterateAllElements() as $item) {
                $customerClient = $item->getCustomerClient();

                if ($customerClient->getId() === $rootCustomerId) {
                    $rootCustomerClient = $customerClient;
                    self::$rootCustomerClients[$rootCustomerId] = $rootCustomerClient;
                }

                if ($customerClient->getId() === $customerIdToSearch) {
                    continue;
                }

                $customerIdsToChildAccounts[$customerIdToSearch][] = $customerClient;

                if ($customerClient->getManager()) {

                    $alreadyVisited = array_key_exists(
                        $customerClient->getId(),
                        $customerIdsToChildAccounts
                    );

                    if (!$alreadyVisited && $customerClient->getLevel() === 1) {
                        array_push($managerCustomerIdsToSearch, $customerClient->getId());
                    }
                }
            }
        }

        return is_null($rootCustomerClient) ? null
            : [$rootCustomerClient->getId() => $customerIdsToChildAccounts];
    }

    /**
     * > This function returns an array of all the customer IDs that the user has access to
     * 
     * @param GoogleAdsClient googleAdsClient The GoogleAdsClient object that you created in the
     * previous step.
     * 
     * @return array An array of customer IDs that the user has access to.
     */
    private static function getAccessibleCustomers(GoogleAdsClient $googleAdsClient): array
    {
        $accessibleIds = [];
        $customerClient = $googleAdsClient->getCustomerServiceClient();
        $accessibleCustomers = $customerClient->listAccessibleCustomers();

        foreach ($accessibleCustomers->getResourceNames() as $item) {
            $customer = CustomerServiceClient::parseName($item)['customer_id'];
            $accessibleIds[] = intval($customer);
        }
        return $accessibleIds;
    }
}
