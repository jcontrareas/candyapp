<?php

namespace App\Http\Controllers\Invitations;

use App\Http\Controllers\Controller;
use App\Models\GoogleToken;
use Google\Ads\GoogleAds\Lib\OAuth2TokenBuilder;
use Google\Ads\GoogleAds\Lib\V11\GoogleAdsClient;
use Google\Ads\GoogleAds\Lib\V11\GoogleAdsClientBuilder;
use Google\Ads\GoogleAds\Util\V11\ResourceNames;
use Google\Ads\GoogleAds\V11\Services\CustomerClientLinkOperation;
use Google\Ads\GoogleAds\V11\Enums\ManagerLinkStatusEnum\ManagerLinkStatus;
use Google\Ads\GoogleAds\V11\Resources\CustomerManagerLink;
use Google\Ads\GoogleAds\V11\Resources\CustomerClientLink;
use Google\Ads\GoogleAds\V11\Services\CustomerManagerLinkOperation;
use Google\Ads\GoogleAds\Util\FieldMasks;
use Google\Client;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class VinculationController extends Controller
{
    private const MANAGER_CUSTOMER_ID = '6963202607';
    private const PAGE_SIZE = 50;
    private const SCOPE = [
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/adwords',
    ];

    // AUTENTICACIÓN Y PERMISOS DE USUARIO CLIENTE
    private static function createGoogleAdsClient(int $customerId)
    {
        $client = GoogleToken::all()->last();
        $oAuth2Credential = (new OAuth2TokenBuilder())
            ->withClientId($client->client_id)
            ->withClientSecret($client->client_secret)
            ->withRefreshToken($client->refresh_token)
            ->build();

        return (new GoogleAdsClientBuilder())
            ->fromFile()
            ->withOAuth2Credential($oAuth2Credential)
            ->withLoginCustomerId($customerId)
            ->build();
    }

    // CREAR NUEVO CLIENTE
    private static function createClient()
    {
        $client =  new Client();
        $client->setApplicationName('GoogleAds');
        $client->setScopes(self::SCOPE);
        $client->setAuthConfig(base_path() . '/client_secret.json');
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        return $client;
    }

    //SOLICITAR CONCENTIMIENTO
    public  function  getConsent()
    {
        $authUrl =  self::createClient()->createAuthUrl();
        return response()->json($authUrl);
    }

    //OBTENIENDO TOKE Y GUARDANDO EN BASE DE DATO
    public  function  storeToken(Request $request)
    {
        $client = self::createClient();
        $code = $request->only('code');
        $token =  $client->fetchAccessTokenWithAuthCode($code['code']);

        GoogleToken::updateOrCreate([
            'client_id' => $this->client->getClientId(),
            'client_secret' => $this->client->getClientSecret(),
            'refresh_token' => $this->client->getRefreshToken()
        ], $token);

        return Redirect::to('http://localhost:4200/dashboard/vinculation');
    }

    //PROCESO DE VINCULACIÓN DE CUENTAS
    public function acceptInvitation(Request $request, GoogleAdsClient $googleAdsClient)
    {
        //ENVIAR INVITACIÓN
        $clientLink = new CustomerClientLink([
            'client_customer' => ResourceNames::forCustomer($request->customerId),
            'status' => ManagerLinkStatus::PENDING
        ]);

        $linkOperation = new CustomerClientLinkOperation();
        $linkOperation->setCreate($clientLink);

        $linkServiceClient = $googleAdsClient->getCustomerClientLinkServiceClient();
        $response = $linkServiceClient->mutateCustomerClientLink(
            self::MANAGER_CUSTOMER_ID,
            $linkOperation
        );

        $linkResourceName = $response->getResult()->getResourceName();

        //OBTENER LINK 
        $query = "SELECT customer_client_link.manager_link_id FROM customer_client_link" .
            " WHERE customer_client_link.resource_name = '$linkResourceName'";

        $serviceClient = $googleAdsClient->getGoogleAdsServiceClient();

        $response = $serviceClient->search(
            self::MANAGER_CUSTOMER_ID,
            $query,
            ['pageSize' => self::PAGE_SIZE]
        );

        $managerLinkId = $response->getIterator()->current()
            ->getCustomerClientLink()
            ->getManagerLinkId();

        $managerLinkResourceName = ResourceNames::forCustomerManagerLink(
            $request->customerId,
            self::MANAGER_CUSTOMER_ID,
            $managerLinkId
        );

        // Acepta la invitación del gestor mientras se autentifica como cliente.
        $newgoogleAdsClient = self::createGoogleAdsClient($request->customerId);

        // Crea el enlace del gestor de clientes con el estado actualizado.
        $customerManagerLink = new CustomerManagerLink();
        $customerManagerLink->setResourceName($managerLinkResourceName);
        $customerManagerLink->setStatus(ManagerLinkStatus::ACTIVE);

        // Crea una operación de enlace del administrador del clientes para actualizar la anterior.
        $customerManagerLinkOperation = new CustomerManagerLinkOperation();
        $customerManagerLinkOperation->setUpdate($customerManagerLink);
        $customerManagerLinkOperation->setUpdateMask(
            FieldMasks::allSetFieldsOf($customerManagerLink)
        );
        
        // Emite una solicitud de mutación para actualizar el enlace de la cuenta administrador del clientes..
        $customerManagerLinkServiceClient =  $newgoogleAdsClient->getCustomerManagerLinkServiceClient();
        $response = $customerManagerLinkServiceClient->mutateCustomerManagerLink(
            $request->customerId,
            [$customerManagerLinkOperation]
        );

        return response()->json([
            'message' => '¡Vinculación realizada con exito!'
        ]);

    }
}
