<?php

namespace App\Http\Controllers\Invitations;

use App\Http\Controllers\Controller;
use App\Http\Resources\Home\CustomersResources;
use App\Models\GoogleToken;
use Google\Ads\GoogleAds\Lib\OAuth2TokenBuilder;
use Google\Ads\GoogleAds\Lib\V11\GoogleAdsClient;
use Google\Ads\GoogleAds\Lib\V11\GoogleAdsClientBuilder;
use Google\Ads\GoogleAds\Util\V11\ResourceNames;
use Google\Ads\GoogleAds\V11\Services\CustomerClientLinkOperation;
use Google\Ads\GoogleAds\V11\Enums\ManagerLinkStatusEnum\ManagerLinkStatus;
use Google\Ads\GoogleAds\V11\Resources\CustomerManagerLink;
use Google\Ads\GoogleAds\V11\Resources\CustomerClientLink;
use Google\Ads\GoogleAds\V11\Services\CustomerManagerLinkOperation;
use Google\Ads\GoogleAds\Util\FieldMasks;
use Google\Client;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class InvitationsController extends Controller
{
    private const MANAGER_CUSTOMER_ID = '6963202607';
    private const CLIENT_CUSTOMER_ID = '8563203974';
    private const PAGE_SIZE = 50;

    public function sendInvitation(Request $request, GoogleAdsClient $googleAdsClient)
    {

        //ENVIAR INVITACIÓN

        $customerClientLink = new CustomerClientLink([
            'client_customer' => ResourceNames::forCustomer($request->customerId),
            'status' => ManagerLinkStatus::PENDING
        ]);
        $customerClientLinkOperation = new CustomerClientLinkOperation();
        $customerClientLinkOperation->setCreate($customerClientLink);

        $customerClientLinkServiceClient = $googleAdsClient->getCustomerClientLinkServiceClient();
        $response = $customerClientLinkServiceClient->mutateCustomerClientLink(
            self::MANAGER_CUSTOMER_ID,
            $customerClientLinkOperation
        );

        $customerClientLinkResourceName = $response->getResult()->getResourceName();

        //OBTENER LINK 

        $query = "SELECT customer_client_link.manager_link_id FROM customer_client_link" .
            " WHERE customer_client_link.resource_name = '$customerClientLinkResourceName'";

        $googleAdsServiceClient = $googleAdsClient->getGoogleAdsServiceClient();

        $response = $googleAdsServiceClient->search(
            self::MANAGER_CUSTOMER_ID,
            $query,
            ['pageSize' => self::PAGE_SIZE]
        );

        $managerLinkId = $response->getIterator()->current()
            ->getCustomerClientLink()
            ->getManagerLinkId();

        $managerLinkResourceName = ResourceNames::forCustomerManagerLink(
            $request->customerId,
            self::MANAGER_CUSTOMER_ID,
            $managerLinkId
        );

        // Acepta la invitación del gestor mientras se autentifica como cliente.

        $newgoogleAdsClient = self::createGoogleAdsClient($request->customerId);

        // Creates the customer manager link with the updated status.
        $customerManagerLink = new CustomerManagerLink();
        $customerManagerLink->setResourceName($managerLinkResourceName);
        $customerManagerLink->setStatus(ManagerLinkStatus::ACTIVE);


        // Creates a customer manager link operation for updating the one above.
        $customerManagerLinkOperation = new CustomerManagerLinkOperation();
        $customerManagerLinkOperation->setUpdate($customerManagerLink);
        $customerManagerLinkOperation->setUpdateMask(
            FieldMasks::allSetFieldsOf($customerManagerLink)
        );

        // Issues a mutate request to update the customer manager link.
        $customerManagerLinkServiceClient =  $newgoogleAdsClient->getCustomerManagerLinkServiceClient();

        $response = $customerManagerLinkServiceClient->mutateCustomerManagerLink(
            $request->customerId,
            [$customerManagerLinkOperation]
        );

        return $response->getResults()[0]->getResourceName();
    }

    private static function createGoogleAdsClient(int $customerId)
    {
        $client = GoogleToken::all()->last();

        $oAuth2Credential = (new OAuth2TokenBuilder())
            ->withClientId($client->client_id)
            ->withClientSecret($client->client_secret)
            ->withRefreshToken($client->refresh_token)
            ->build();

        return (new GoogleAdsClientBuilder())
            ->fromFile()
            ->withOAuth2Credential($oAuth2Credential)
            ->withLoginCustomerId($customerId)
            ->build();
    }

    public  function  getConsent()
    {
        $scopes = [
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/adwords',
        ];


        $this->client =  new Client();
        $this->client->setApplicationName('GoogleAds');
        $this->client->setScopes($scopes);
        $this->client->setAuthConfig(base_path() . '/client_secret.json');
        $this->client->setAccessType('offline');
        $this->client->setPrompt('select_account consent');
        $authUrl =  $this->client->createAuthUrl();
        // return  $authUrl;
        return response()->json($authUrl);
    }

    public  function  storeToken(Request $request)
    {
        $scopes = [
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/adwords',
        ];

        $this->client =  new  Client();
        $this->client->setApplicationName('GoogleAds');
        $this->client->setScopes($scopes);
        $this->client->setAuthConfig(base_path() . '/client_secret.json');
        $this->client->setAccessType('offline');
        $this->client->setPrompt('select_account consent');

        $code = $request->only('code');

        $token =  $this->client->fetchAccessTokenWithAuthCode($code['code']);

        GoogleToken::updateOrCreate([
            'client_id' => $this->client->getClientId(),
            'client_secret' => $this->client->getClientSecret(),
            'refresh_token' => $this->client->getRefreshToken()
        ], $token);

        return Redirect::to('http://localhost:4200/dashboard/vinculation');
    }

    public function getAccesibleCustomer()
    {
        $customers = array(
            (object) [
                'id' => '5916590184',
                'name' => 'Suministros M&R'
            ],
            (object) [
                'id' => '8563203974',
                'name' => 'Jose Alcala Test'
            ]
        );

        return CustomersResources::make($customers);
    }

    public function acceptInvitation(Request $request, GoogleAdsClient $googleAdsClient)
    {
        //ENVIAR INVITACIÓN

        $customerClientLink = new CustomerClientLink([
            'client_customer' => ResourceNames::forCustomer($request->customerId),
            'status' => ManagerLinkStatus::PENDING
        ]);

        $customerClientLinkOperation = new CustomerClientLinkOperation();
        $customerClientLinkOperation->setCreate($customerClientLink);

        $customerClientLinkServiceClient = $googleAdsClient->getCustomerClientLinkServiceClient();
        $response = $customerClientLinkServiceClient->mutateCustomerClientLink(
            self::MANAGER_CUSTOMER_ID,
            $customerClientLinkOperation
        );

        $customerClientLinkResourceName = $response->getResult()->getResourceName();

        //OBTENER LINK 

        $query = "SELECT customer_client_link.manager_link_id FROM customer_client_link" .
            " WHERE customer_client_link.resource_name = '$customerClientLinkResourceName'";

        $googleAdsServiceClient = $googleAdsClient->getGoogleAdsServiceClient();

        $response = $googleAdsServiceClient->search(
            self::MANAGER_CUSTOMER_ID,
            $query,
            ['pageSize' => self::PAGE_SIZE]
        );

        $managerLinkId = $response->getIterator()->current()
            ->getCustomerClientLink()
            ->getManagerLinkId();

        $managerLinkResourceName = ResourceNames::forCustomerManagerLink(
            $request->customerId,
            self::MANAGER_CUSTOMER_ID,
            $managerLinkId
        );

        // Acepta la invitación del gestor mientras se autentifica como cliente.

        $newgoogleAdsClient = self::createGoogleAdsClient($request->customerId);

        // Creates the customer manager link with the updated status.
        $customerManagerLink = new CustomerManagerLink();
        $customerManagerLink->setResourceName($managerLinkResourceName);
        $customerManagerLink->setStatus(ManagerLinkStatus::ACTIVE);

        
        
        // Creates a customer manager link operation for updating the one above.
        $customerManagerLinkOperation = new CustomerManagerLinkOperation();
        $customerManagerLinkOperation->setUpdate($customerManagerLink);
        $customerManagerLinkOperation->setUpdateMask(
            FieldMasks::allSetFieldsOf($customerManagerLink)
        );
        
        
        // Issues a mutate request to update the customer manager link.
        $customerManagerLinkServiceClient =  $newgoogleAdsClient->getCustomerManagerLinkServiceClient();
        
        
        $response = $customerManagerLinkServiceClient->mutateCustomerManagerLink(
            $request->customerId,
            [$customerManagerLinkOperation]
        );

        return response()->json([
            'message' => '¡Vinculación realizada con exito!'
        ]);

    }
}
