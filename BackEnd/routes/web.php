<?php

use App\Http\Controllers\HomeController\HomeController;
use App\Http\Controllers\Invitations\InvitationsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', [HomeController::class, 'getAccount']);
// Route::get('getCampaigns', [HomeController::class, 'getCampaings'])->name('campaings');
// Route::get('getAddGroups', [HomeController::class, 'getAdGroups'])->name('adGroups');
// Route::get('getKeywords', [HomeController::class, 'getKeywords'])->name('keywords');

Route::get('/vincular', [InvitationsController::class, 'storeToken'])->name('invitations');

// Route::get('/vincular', function () {
//     return view('vincular');
// })->name('vincular');
