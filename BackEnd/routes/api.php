<?php

use App\Http\Controllers\Home\HomeController;
use App\Http\Controllers\Invitations\InvitationsController;
use App\Http\Controllers\Invitations\VinculationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/home/customers', [HomeController::class, 'getAccount'])->name('getAccount');

Route::get('/home/campaigns', [HomeController::class, 'getCampaings'])->name('getCampaings');

Route::get('/home/ad-groups', [HomeController::class, 'getAdGroups'])->name('getAdGroups');

Route::get('/home/keywords', [HomeController::class, 'getKeywords'])->name('getKeywords');

Route::post('/vincular', [InvitationsController::class, 'sendInvitation'])->name('invitations');

Route::get('/linking/getConsent', [InvitationsController::class, 'getConsent'])->name('getConsent');

Route::get('/accesible/customers', [InvitationsController::class, 'getAccesibleCustomer'])->name('getAccesibleCustomer');

Route::post('/init/vinculation', [VinculationController::class, 'acceptInvitation'])->name('acceptInvitation');



Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
